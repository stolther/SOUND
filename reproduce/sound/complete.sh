#!/usr/bin/env bash
#conda activate soundflink

# Figure 4
./reproduce/sound/figure4_left.sh 5 1.5
./reproduce/sound/figure4_right.sh 5 1.5

# Figure 5
./reproduce/sound/overhead_CI_astro.sh 5 1.5
./reproduce/sound/overhead_maxsamples_astro.sh 5 1.5
./reproduce/sound/overhead_CI_smartgrid.sh 5 1.5
./reproduce/sound/overhead_maxsamples_smartgrid.sh 5 1.5

# Figure 6
./reproduce/sound/figure6_left_checkresult_uncertainty_case.sh 1 4
./reproduce/sound/figure6_right_checkresult_n_samples_case.sh 1 4
./reproduce/sound/figure6_right_checkresult_n_samples_caseB.sh 1 4

# Figure 7
./reproduce/sound/figure7_checkresult_sparsity_case.sh 1 4

# Figure 8
./reproduce/sound/figure8.sh 1 4

# Table V

./reproduce/sound/figure_checkresult_comparison_NOSOUND_case.sh 1 8
./reproduce/sound/smartgrid_NOSOUND_case.sh 1 8

# Table VI + Figure 9
./reproduce/sound/figure10.sh 1 1.5
