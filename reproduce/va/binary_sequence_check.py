from dataclasses import dataclass

import numpy as np

from scipy.stats import beta

OUTCOME_INCONCLUSIVE = 0
OUTCOME_SATISFIED = 1
OUTCOME_VIOLATED = 2

OUTCOMES = {
    0: "INCONCLUSIVE",
    1: "SATISFIED",
    2: "VIOLATED",
}


class DataSeries:
    v: np.ndarray
    sigma: np.ndarray

    def __init__(self, v: np.ndarray, sigma: np.ndarray):
        self.v = v
        self.sigma = sigma

        if sigma is None:
            self.sigma = np.zeros_like(v)


def binary_sequence_check(
    constraint,
    s1: DataSeries,
    s2: DataSeries,
    n_samples,
    decision_lookup_table,
    value_uncertainty_n_sigma=2.0,
):
    n_satisfied = 0
    n_violated = 0
    n_samples_seen = 1
    outcome = 0

    n_observations = len(s1.v)
    block_length = int(np.floor(np.sqrt(n_observations)))
    num_blocks = int(np.floor(n_observations / block_length))

    bootstrap_sample1 = np.zeros(n_observations)
    bootstrap_sample2 = np.zeros(n_observations)

    for _ in range(n_samples):
        sample_index = 0

        for _ in range(num_blocks):
            random_block_start = np.random.randint(0, n_observations - block_length + 1)

            for i in range(block_length):
                current_index = random_block_start + i

                bootstrap_sample1[sample_index] = sample_value(
                    s1, current_index, value_uncertainty_n_sigma
                )
                bootstrap_sample2[sample_index] = sample_value(
                    s2, current_index, value_uncertainty_n_sigma
                )

                sample_index += 1

        result = constraint(bootstrap_sample1, bootstrap_sample2)
        n_satisfied += 1 if result else 0
        n_violated = n_samples_seen - n_satisfied
        outcome = decision_lookup_table[1 + n_satisfied][1 + n_violated]
        if outcome > 0:
            break

        n_samples_seen += 1

    return {
        "naive_outcome": "SATISFIED" if constraint(s1.v, s2.v) else "VIOLATED",
        "outcome": OUTCOMES[outcome],
        "alpha": 1 + n_violated,
        "beta": 1 + n_satisfied,
        "n_samples_seen": n_samples_seen,
    }


def sample_value(series: DataSeries, index, value_uncertainty_n_sigma):
    if series.sigma is not None:
        return np.random.normal(
            series.v[index], series.sigma[index] * value_uncertainty_n_sigma
        )
    return series.v[index]


def A3(x: np.ndarray, y: np.ndarray) -> bool:
    if x.size < 2 or y.size < 2:
        return True

    if not np.isfinite(x).all() or not np.isfinite(y).all():
        print(x, y)
    with np.errstate(invalid="ignore"):
        corr = np.corrcoef(x, y)[0, 1]
    if np.isnan(corr):
        return True
    else:
        return corr > 0.2


def A4(x: np.ndarray, y: np.ndarray) -> bool:
    def average_delta(array: np.ndarray) -> float:
        if len(array) < 2:
            raise ValueError("Array should have at least two elements.")
        sum_delta = np.sum(np.abs(np.diff(array)))
        return sum_delta / (len(array) - 1)

    avg_delta_x = average_delta(x)
    avg_delta_y = average_delta(y)
    return avg_delta_x < avg_delta_y


def create_lookup_table(max_sample_size, credibility_level, decision_threshold):
    max_sample_size += 1

    lookup_table = np.zeros((max_sample_size + 1, max_sample_size + 1), dtype=int)

    for alpha_post in range(1, max_sample_size + 1):
        for beta_post in range(1, max_sample_size - alpha_post + 2):
            beta_dist = beta(alpha_post, beta_post)
            lower_bound = beta_dist.ppf((1 - credibility_level) / 2)
            upper_bound = beta_dist.ppf(1 - (1 - credibility_level) / 2)

            if lower_bound > decision_threshold:
                lookup_table[alpha_post][beta_post] = OUTCOME_SATISFIED
            elif upper_bound < decision_threshold:
                lookup_table[alpha_post][beta_post] = OUTCOME_VIOLATED
            else:
                lookup_table[alpha_post][beta_post] = OUTCOME_INCONCLUSIVE

    return lookup_table


CONSTRAINTS = {
    "A-3": A3,
    "A-4": A4,
}
