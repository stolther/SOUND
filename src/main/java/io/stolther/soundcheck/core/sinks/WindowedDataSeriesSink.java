package io.stolther.soundcheck.core.sinks;

import io.palyvos.provenance.util.ExperimentSettings;
import io.stolther.soundcheck.core.DataSeries;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.functions.sink.RichSinkFunction;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;

public class WindowedDataSeriesSink extends RichSinkFunction<DataSeries> {
    private final ExperimentSettings settings;
    private transient PrintWriter pw_v;
    private transient PrintWriter pw_t;
    private transient PrintWriter pw_s;
    private final String name;


    public WindowedDataSeriesSink(String name, ExperimentSettings settings) {
        this.settings = settings;
        this.name = name;
    }

    @Override
    public void open(Configuration parameters) throws Exception {
        final int taskIndex = getRuntimeContext().getIndexOfThisSubtask();
        try {
            pw_v = new PrintWriter(new FileWriter(settings.windowSeriesFile(taskIndex, name, "v")), settings.autoFlush());
            pw_t = new PrintWriter(new FileWriter(settings.windowSeriesFile(taskIndex, name, "t")), settings.autoFlush());
            pw_s = new PrintWriter(new FileWriter(settings.windowSeriesFile(taskIndex, name, "s")), settings.autoFlush());
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        super.open(parameters);

    }

    @Override
    public void invoke(DataSeries ds, Context context) {
        pw_v.println(Arrays.toString(ds.v)
                .replace("[", "")
                .replace("]", "")
                .trim());

        pw_t.println(Arrays.toString(new double[]{ds.t})
                .replace("[", "")
                .replace("]", "")
                .trim());

        pw_s.println(Arrays.toString(ds.sigma)
                .replace("[", "")
                .replace("]", "")
                .trim());

    }


}
